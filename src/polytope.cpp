/**
 * @authors Satalia team.
 * @brief Implements class responsible for computing generators
 */

#include "polytope.h"
#include "openspace_types.h"
#include "ppl.hh"
#include <algorithm>
#include <range/v3/all.hpp>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>

using std::cbegin;
using std::cend;
using std::string;
using std::vector;

namespace RV = ranges::view;
namespace PPL = Parma_Polyhedra_Library;

namespace openspace {

Polytope::Polytope(StringVec centroids) {
  for (const auto &centroid : centroids) {
    auto other_centroids = centroids | RV::remove_if([&centroid](const auto &other) { return centroid==other; });
    ranges::for_each(other_centroids, [this, &centroid](const auto &other) {
      variables_.insert({StringPair(other, centroid), PPL::Variable(variables_.size())});
    });
  }
}

void Polytope::addFlowConstraints(StringVec centroids,
                                  StringValueMap centroid_demands,
                                  StringValueMap centroid_supplies) {
  for (const auto &centroid : centroids) {
    addSupplyConstraint(centroid, centroid_supplies.at(centroid), centroids);
    addDemandConstraint(centroid, centroid_demands.at(centroid), centroids);
  }
}

PPL::Linear_Expression Polytope::getSumExpr(std::string vertex, const StringVec &vertices, bool outgoing) {
  auto expr = PPL::Linear_Expression::zero();
  auto other_vertices = vertices | RV::remove_if([&vertex](const auto &other) { return other==vertex; });
  for (const auto &other : other_vertices) {
    expr += outgoing ? variables_.at({vertex, other}) : variables_.at({other, vertex});
  }
  return expr;
}

void Polytope::addNonNegativityConstraints() {
  for (auto &variable : variables_ | RV::values) {
    constraints_.insert(variable >= 0);
  }
}

void Polytope::addSupplyConstraint(const string &centroid,
                                   ValueType supply,
                                   const StringVec &centroids) {
  auto lhs = getSumExpr(centroid, centroids, true);
  constraints_.insert(lhs==supply);
}

void Polytope::addDemandConstraint(const string &centroid,
                                   ValueType demand,
                                   const StringVec &centroids) {
  auto lhs = getSumExpr(centroid, centroids, false);
  constraints_.insert(lhs==demand);
}

void Polytope::computeGenerators() {
  auto polyhedron = PPL::C_Polyhedron(constraints_);
  const auto &generators = polyhedron.minimized_generators();
  generators_.clear();
  std::transform(cbegin(generators), cend(generators), std::back_inserter(generators_),
                 [this](auto &generator) { return Point(variables_, generator); });
}

std::size_t Polytope::getAffineDimension() const {
  auto polyhedron = PPL::C_Polyhedron(constraints_);
  return polyhedron.affine_dimension();
}

void Polytope::printGenerators() const {
  std::ostream_iterator<Point> out(std::cout, "\n");
  std::copy(cbegin(generators_), cend(generators_), out);
}

vector<StringPair> Polytope::getVariables() const {
  auto vars = vector<StringPair>{};
  std::transform(cbegin(variables_), cend(variables_),
                 std::back_inserter(vars), [](auto &pair) { return pair.first; });
  return vars;
}

void Polytope::addPathPreferenceConstraints() {

}

}

