/**
 * @authors Satalia team.
 * @brief Represents a point in the polytope
 */

#include "point.h"
#include "gsl/gsl-lite.hpp"
#include "gmpxx.h"
#include <algorithm>
#include <iterator>
#include <ostream>
#include <point.h>
#include <sstream>
#include <string>
#include <typeinfo>
#include <utility>

using std::cbegin;
using std::cend;
using std::string;

namespace openspace {

std::ostream &operator<<(std::ostream &ostream, const Point &point) {
  ostream << "Point( ";
  std::for_each(cbegin(point.variables_), cend(point.variables_),
                [&ostream](auto &pair) {
                  const auto&[u, v] = pair.first;
                  ostream << u << "-" << v << "=" << pair.second << " ";
                });
  ostream << ")";
  return ostream;
}

Point::Point(std::map<StringPair, double> pairs)
    : variables_(std::move(pairs)) {}

Point::Point(const std::map<StringPair, PPL::Variable> &variables, const PPL::Generator &generator) {
  if (!generator.is_point()) {
    std::stringstream message("Generator is not a point.\n");
    throw std::runtime_error(message.str());
  }
  auto inserter = std::inserter(variables_, variables_.end());
  std::transform(cbegin(variables), cend(variables), inserter,
                 [&generator](auto &pair) {
                   auto value = generator.coefficient(pair.second).get_d(); // coefficient is of type mpz_class
                   return std::make_pair(pair.first, value);
                 });
}

Point::Point(const string &file) {
  auto input = std::ifstream(file);
  if (!input.good()) {
    std::stringstream message("Couldn't open point file.\n");
    throw std::runtime_error(message.str());
  }
  auto values = readLine(input);
  while (!values.empty()) {
    if (values.size()!=3) {
      std::stringstream message("Unexpected point input line.\n");
      throw std::runtime_error(message.str());
    }
    auto string_pair = StringPair(values.at(0), values.at(1));
    auto matrix_value = std::stod(values.at(2));
    variables_.insert({string_pair, matrix_value});
    values = readLine(input);
  }
}

double Point::getValue(const StringPair &pair) const {
  return variables_.at(pair);
}

}