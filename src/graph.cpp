#include "graph.h"

#include <boost/algorithm/string.hpp>
#include "gsl/gsl-lite.hpp"
#include "ppl.hh"
#include <algorithm>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <range/v3/all.hpp>

namespace PPL = Parma_Polyhedra_Library;
namespace RV = ranges::view;

using std::begin;
using std::end;
using std::stof;
using std::string;

namespace openspace {

Graph::Graph(const string &file)
    : arc_flows_(nullptr) {
  auto input = std::ifstream(file);
  if (!input.good()) {
    std::stringstream message("Couldn't open input file.\n");
    throw std::runtime_error(message.str());
  }
  readInput(input);
}

void Graph::readInput(std::istream &input) {
  auto first_line = readLine(input);
  number_of_nodes_ = gsl::narrow<int>(stof(first_line.at(0)));
  auto second_line = readLine(input);
  number_of_centroids_ = gsl::narrow<int>(stof(second_line.at(0)));
  readCentroidDemandAndSupply(input);
  readEdgeFlows(input);
  createLemonGraph();
  readPathPreferences(input);
}

void Graph::readPathPreferences(std::istream &input) {
  auto values = readLine(input);
  while (!values.empty()) {
    if (values.size()!=2) {
      std::stringstream message("Unexpected input for path preferences.\n");
      throw std::runtime_error(message.str());
    }
    auto nodes_on_path = StringVec{};
    boost::split(nodes_on_path, values.at(0), boost::is_any_of(","));
    auto lemon_path = createPath(nodes_on_path);
    auto percentage = stof(values.at(1));
    preferred_paths_.insert({lemon_path, percentage});
    values = readLine(input);
  }
}

LemonPath Graph::createPath(StringVec nodes) const {
  auto path = LemonPath();
  for (auto u = begin(nodes); u!=std::prev(end(nodes)); ++u) {
    auto v = std::next(u);
    const auto &arc = graph_arcs_.left.at(StringPair(*u, *v));
    path.addBack(arc);
  }
  return path;
}

void Graph::createLemonGraph() {
  std::transform(begin(nodes_), end(nodes_), std::insert_iterator(graph_nodes_, end(graph_nodes_)),
                 [this](const auto &node) {
                   auto lemon_node = graph_.addNode();
                   return nodeBimap::value_type(node, lemon_node);/*std::make_pair(node, lemon_node);*/
                 });
  for (const auto &arc : edges_to_flow | RV::keys) {
    const auto &[u, v] = arc;
    auto lemon_arc = graph_.addArc(graph_nodes_.left.at(u), graph_nodes_.left.at(v));
    graph_arcs_.insert(arcBimap::value_type(arc, lemon_arc));
  }
  arc_flows_ = std::make_unique<LemonArcMap>(graph_);
  for (const auto &[arc, flow] : edges_to_flow) {
    arc_flows_->set(graph_arcs_.left.at(arc), flow);
  }
}

void Graph::readCentroidDemandAndSupply(std::istream &input) {
  for (auto i = 0; i < number_of_centroids_; ++i) {
    auto line = readLine(input);
    auto centroid = line.at(0);
    centroids_.push_back(centroid);
    auto demand = gsl::narrow<ValueType>(stof(line.at(1)));
    auto supply = gsl::narrow<ValueType>(stof(line.at(2)));
    if (demand > 0) {
      std::stringstream message("Expected non-positive demand: " + std::to_string(demand) + "\n");
      throw std::runtime_error(message.str());
    } else if (supply < 0) {
      std::stringstream message("Expected non-negative supply: " + std::to_string(supply) + "\n");
      throw std::runtime_error(message.str());
    }
    centroid_to_demand[centroid] = -demand; // we consider demand as non-negative value from here on
    centroid_to_supply[centroid] = supply;
  }
}

void Graph::readEdgeFlows(std::istream &input, string delimiter) {
  for (auto i = 0; i < number_of_nodes_; ++i) {
    auto line = readLine(input);
    auto outgoing_vertex = line.at(0);
    nodes_.push_back(outgoing_vertex);
    std::transform(std::next(begin(line)), end(line), std::inserter(edges_to_flow, end(edges_to_flow)),
                   [outgoing_vertex, delimiter](const string &token) {
                     auto values = StringVec{};
                     boost::split(values, token, boost::is_any_of(delimiter));
                     auto incoming_vertex = values.at(0);
                     auto flow_value = gsl::narrow<ValueType>(stof(values.at(1)));
                     return std::make_pair(StringPair(outgoing_vertex, incoming_vertex), flow_value);
                   });
  }
}

}