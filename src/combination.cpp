/**
 * @authors Satalia team.
 * @brief
 */

#include "combination.h"
#include "point.h"
#include "xprb_cpp.h"
#include <cstddef>
#include <iterator>
#include <utility>
#include <vector>

using std::cbegin;
using std::cend;
using std::pair;
using std::size_t;
using std::vector;

namespace xp = dashoptimization;

namespace openspace {

Combination::Combination(vector<StringPair> variables,
                         vector<Point> generators)
    : variables_(std::move(variables)),
      generators_(std::move(generators)) {}

Point Combination::getCombination(const PointCombination &lambda) const {
  return generators_.front();
}

vector<pair<Point, double>> Combination::computeLambda(const openspace::Point &point) const {
  xp::XPRBprob problem("compute_lambda");
  auto vars = createVariables(problem);
  addSumOneConstraint(problem, vars);
  addPointConstraints(problem, vars, point);
  problem.setMsgLevel(0);
  problem.solve();
  if (problem.getMIPStat()!=XPRB_MIP_LP_OPTIMAL) {
    std::stringstream message("Non-optimal computation regarding lambda combination.\n");
    throw std::runtime_error(message.str());
  }
  auto combination = vector<pair<Point, double>>{};
  for (size_t i = 0; i < vars.size(); ++i) {
    auto solution_value = vars.at(i).getSol();
    if (solution_value > 0.) {
      combination.push_back({generators_.at(i), solution_value});
    }
  }
  return combination;
}

vector<xp::XPRBvar> Combination::createVariables(xp::XPRBprob &problem) const {
  auto vars = vector<xp::XPRBvar>{};
  for (size_t i = 0; i < generators_.size(); ++i) {
    auto name = "lambda_" + std::to_string(i);
    vars.push_back(problem.newVar(name.c_str(), XB_PL, 0., 1.));
  }
  return vars;
}

void Combination::addSumOneConstraint(xp::XPRBprob &problem,
                                      const vector<xp::XPRBvar> &variables) const {
  auto lhs = xp::XPRBexpr();
  std::for_each(cbegin(variables), cend(variables),
                [&lhs](auto &var) {
                  lhs.add(var);
                });
  problem.newCtr("lambda_equal_one", lhs==1.);
}

void Combination::addPointConstraints(xp::XPRBprob &problem,
                                      const vector<xp::XPRBvar> &variables, const Point &point) const {
  for (const auto &string_pair : variables_) {
    addPointConstraint(problem, variables, string_pair, point.getValue(string_pair));
  }
}

void Combination::addPointConstraint(xp::XPRBprob &problem,
                                     const vector<xp::XPRBvar> &variables,
                                     const StringPair &string_pair,
                                     double rhs) const {
  auto lhs = xp::XPRBexpr();
  for (size_t i = 0; i < variables.size(); ++i) {
    auto coefficient = generators_.at(i).getValue(string_pair);
    lhs.addTerm(coefficient, variables.at(i));
  }
  auto name = string_pair.first + string_pair.second;
  problem.newCtr(name.c_str(), lhs==rhs);
}

}