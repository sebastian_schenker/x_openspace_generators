#include "combination.h"
#include "graph.h"
#include "polytope.h"
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <utility>

using namespace openspace;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::stringstream message("Input file as command line argument expected.\n");
    throw std::runtime_error(message.str());
  }
  auto input = Graph(argv[1]);
  auto matrix_polytope = Polytope(input.getCentroids());
  matrix_polytope.addNonNegativityConstraints();
  matrix_polytope.addFlowConstraints(input.getCentroids(), input.getCentroidDemands(), input.getCentroidSupplies());
  matrix_polytope.addPathPreferenceConstraints();
  matrix_polytope.computeGenerators();
  std::cout << "Dimension of O-D matrix polytope: " << matrix_polytope.getDimension() << "\n";
  std::cout << "Affine dimension of O-D matrix polytope: " << matrix_polytope.getAffineDimension() << "\n";
  std::cout << "Number of generators: " << matrix_polytope.getNumberOfGenerators() << "\n";
  std::unique_ptr<Point> matrix;
  if (argc > 2) {
    matrix = std::make_unique<Point>(argv[2]);
  }
  auto conv_comb = Combination(std::move(matrix_polytope.getVariables()), std::move(matrix_polytope.getGenerators()));
  auto combination = conv_comb.computeLambda(*matrix);
  return 0;
}