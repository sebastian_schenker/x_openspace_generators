/**
 * @authors Satalia team.
 * @brief Class responsible for graph structure
 */

#ifndef OPENSPACE_GRAPH_H_INC
#define OPENSPACE_GRAPH_H_INC

#include "openspace_types.h"
#include "ppl.hh"
#include <boost/bimap.hpp>
#include <fstream>
#include <memory>
#include <string>
#include <map>
#include <vector>

namespace PPL = Parma_Polyhedra_Library;

namespace openspace {

class Graph {
 public:
  Graph(const std::string &filename);

  StringVec getCentroids() const {
    return centroids_;
  }

  StringVec getNodes() const {
    return nodes_;
  }

  StringValueMap getCentroidDemands() const {
    return centroid_to_demand;
  }

  StringValueMap getCentroidSupplies() const {
    return centroid_to_supply;
  }

 private:
  using nodeBimap = boost::bimap<std::string, LemonNode>;
  using arcBimap = boost::bimap<StringPair, LemonArc>;

  LemonGraph graph_;
  int number_of_nodes_;
  int number_of_centroids_;
  StringVec centroids_;
  StringVec nodes_;
  StringValueMap centroid_to_supply;
  StringValueMap centroid_to_demand;
  StringPairValueMap edges_to_flow;
  nodeBimap graph_nodes_;
  arcBimap graph_arcs_;
  std::unique_ptr<LemonArcMap> arc_flows_;
  std::map<LemonPath, float, LemonPathCompare> preferred_paths_;

  void readInput(std::istream &input);

  void readCentroidDemandAndSupply(std::istream &input);

  void readEdgeFlows(std::istream &input, std::string delimiter = ":");

  void readPathPreferences(std::istream &input);

  LemonPath createPath(StringVec nodes) const;

  void createLemonGraph();
};

}

#endif
