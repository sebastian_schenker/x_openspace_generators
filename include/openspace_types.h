/**
 * @authors Satalia team.
 * @brief Defines project related type aliases.
 */

#ifndef OPENSPACE_GENERATORS_OPENSPACE_TYPES_H
#define OPENSPACE_GENERATORS_OPENSPACE_TYPES_H

#include <boost/algorithm/string.hpp>
#include "lemon/list_graph.h"
#include "lemon/concepts/digraph.h"
#include "lemon/path.h"
#include <map>
#include <fstream>
#include <string>
#include <vector>

namespace openspace {

using ValueType = int;
using StringValueMap = std::map<std::string, ValueType>;
using StringPair = std::pair<std::string, std::string>;
using StringPairValueMap = std::map<StringPair, ValueType>;
using StringVec = std::vector<std::string>;
using LemonGraph = lemon::ListDigraph;
using LemonNode = LemonGraph::Node;
using LemonArc = LemonGraph::Arc;
using LemonArcMap = LemonGraph::ArcMap<ValueType>;
using LemonPath = lemon::Path<LemonGraph>;

struct LemonPathCompare {
  bool operator()(const LemonPath &lhs, const LemonPath &rhs) {
    if (lhs.length()==rhs.length()) {
      for (auto i = 0; i < lhs.length(); ++i) {
        const auto &lhs_arc = lhs.nth(i);
        const auto &rhs_arc = rhs.nth(i);
        if (lhs_arc!=rhs_arc) {
          return lhs_arc < rhs_arc;
        }
      }
      return false;
    }
    return lhs.length() < rhs.length();
  }
};

static StringVec readLine(std::istream &input, std::string tokenDelimiter = " ") {
  auto line = std::string{};
  std::getline(input, line);
  boost::trim(line);
  auto values = StringVec{};
  if (!line.empty()) {
    boost::split(values, line, boost::is_any_of(tokenDelimiter));
  }
  return values;
}

}

#endif //OPENSPACE_GENERATORS_OPENSPACE_TYPES_H
