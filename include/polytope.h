/**
 * @authors Satalia team.
 * @brief Class responsible for computing generators
 */
#ifndef OPENSPACE_GENERATORS_POLYTOPE_H
#define OPENSPACE_GENERATORS_POLYTOPE_H

#include "openspace_types.h"
#include "point.h"
#include "ppl.hh"
#include <cstddef>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace PPL = Parma_Polyhedra_Library;

namespace openspace {

class Polytope {
 public:
  Polytope(StringVec centroids);

  void addNonNegativityConstraints();

  void addFlowConstraints(StringVec centroids, StringValueMap centroid_demands, StringValueMap centroid_supplies);

  void addPathPreferenceConstraints();

  void computeGenerators();

  std::size_t getNumberOfGenerators() const { return generators_.size(); }

  std::size_t getDimension() const { return constraints_.space_dimension(); }

  std::size_t getAffineDimension() const;

  void printGenerators() const;

  std::vector<Point> getGenerators() const { return generators_; }

  std::vector<StringPair> getVariables() const;

 private:

  std::map<StringPair, PPL::Variable> variables_;
  std::vector<Point> generators_;
  PPL::Constraint_System constraints_;

  void addSupplyConstraint(const std::string &centroid, ValueType supply, const StringVec &centroids);

  void addDemandConstraint(const std::string &centroid, ValueType demand, const StringVec &centroids);

  PPL::Linear_Expression getSumExpr(std::string vertex, const StringVec &vertices, bool outgoing);
};

}

#endif //OPENSPACE_GENERATORS_POLYTOPE_H
