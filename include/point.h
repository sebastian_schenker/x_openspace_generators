/**
 * @authors Satalia team.
 * @brief Represents a point in the polytope
 */
#ifndef OPENSPACE_GENERATORS_POINT_H
#define OPENSPACE_GENERATORS_POINT_H

#include "openspace_types.h"
#include "gmpxx.h"
#include "ppl.hh"
#include <map>
#include <ostream>
#include <string>

namespace PPL = Parma_Polyhedra_Library;

namespace openspace {

class Point {
 public:
  Point() = delete;
  Point(std::map<StringPair, double>);
  Point(const std::map<StringPair, PPL::Variable> &, const PPL::Generator &);
  Point(const std::string &file);

  friend std::ostream &operator<<(std::ostream &, const Point &);

  double getValue(const StringPair &pair) const;

 private:
  std::map<StringPair, double> variables_;
};

}

#endif //OPENSPACE_GENERATORS_POINT_H
