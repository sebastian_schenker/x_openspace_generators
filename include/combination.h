/**
 * @authors Satalia team.
 * @brief
 */
#ifndef OPENSPACE_GENERATORS_COMBINATION_H
#define OPENSPACE_GENERATORS_COMBINATION_H

#include "openspace_types.h"
#include "point.h"
#include "xprb_cpp.h"
#include <utility>
#include <vector>

namespace xp = dashoptimization;

namespace openspace {

class Combination {
 public:
  using PointCombination = std::vector<std::pair<Point, double>>;

  Combination() = delete;
  Combination(std::vector<StringPair> variables, std::vector<Point> generators);

  Point getCombination(const PointCombination &lambda) const;

  PointCombination computeLambda(const Point &point) const;

 private:
  std::vector<StringPair> variables_;
  std::vector<Point> generators_;

  std::vector<xp::XPRBvar> createVariables(xp::XPRBprob &problem) const;

  void addSumOneConstraint(xp::XPRBprob &problem, const std::vector<xp::XPRBvar> &variables) const;

  void addPointConstraints(xp::XPRBprob &problem,
                           const std::vector<xp::XPRBvar> &variables,
                           const Point &point) const;

  void addPointConstraint(xp::XPRBprob &problem,
                          const std::vector<xp::XPRBvar> &variables,
                          const StringPair &string_pair, double rhs) const;
};

}

#endif //OPENSPACE_GENERATORS_COMBINATION_H
