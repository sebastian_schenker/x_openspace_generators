# OpenSpace Optimisation
Computing generators for Origin-Destination matrix polytope

## Compilation

We use [Cmake](https://cmake.org). 

Run the following commands in the project folder: 

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

The *cmake* script will consider the following **Environment Variables**: 

* `RANGE_INC` - the include directory of the [Range](https://github.com/ericniebler/range-v3) library
* `BOOST_INC` - the include directory of the [Boost](http://www.boost.org) library
* `PPL_INC` - the include directory of the [PPL](https://www.bugseng.com/parma-polyhedra-library) library
* `PPL_LIB` - the directory containing the PPL library `libppl.so` or `libppl.a`
* `GMP_LIB` - the directory containing the [GMP](https://gmplib.org/) library `libgmp.so` or `libgmp.a`

## Required libraries

### [Range](https://github.com/ericniebler/range-v3) - Range library for C++
- download or clone from github
- no compilation is needed; only header files are used

### [Boost](http://www.boost.org) - Boost library for C++
- only header files are used
- available on Ubuntu; `apt-cache search libboost`

### [PPL](https://www.bugseng.com/parma-polyhedra-library) - Parma Polyhedral Library
- available on Ubuntu; `apt-cache search libppl-dev`

### [GMP](https://gmplib.org/) - GNU Multiple Precision Arithmetic Library
- available on Ubuntu; `apt-cache search libgmp-dev`
- on Ubuntu dependency should be automatically resolved when `libppl-dev` is installed

## Program Execution
Run `openspace` executable with input file as command line parameter, e.g. `./openspace input_file.txt`



 